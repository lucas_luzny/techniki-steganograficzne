package Lab2;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * @author lucas on 04.11.2019
 */
@SuppressWarnings("ALL")
public class App {

    private static final String IMG_PATH = "D:\\Studia\\Semestr 3\\Techniki Steganograficzne\\src\\main\\resources\\SampleImage.BMP";
    private static final String IMG2_PATH = "D:\\Studia\\Semestr 3\\Techniki Steganograficzne\\src\\main\\resources\\SampleImage.BMP";

    private JFrame frame;
    private JFileChooser fileChooser;
    private File selectedFile;
    private JPanel panel;
    private BufferedImage img;
    private ImageIcon icon;
    private JLabel label;
    private BufferedImage img2;
    private ImageIcon icon2;
    private JLabel label2;
    private JButton loadButton;
    private JButton insertButton;
    private JButton readButton;
    private JButton saveButton;
    private JLabel textAreaLabel;
    private JTextArea textArea;
    private JLabel passwordFieldLabel;
    private JTextField passwordField;
    private JLabel keyFieldLabel;
    private JTextField keyField;

    public static void main(String avg[]) throws Exception {
        App app = new App();
        app.initializeButtons();
    }

    private void initializeButtons() {
        frame = new JFrame();
        frame.setSize(1400, 1000);
        frame.setLayout(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("D:\\Studia\\Semestr 3\\Techniki Steganograficzne\\laboratoria\\src\\main\\resources"));
        panel = new JPanel();

        loadButton = new JButton("Wczytaj plik");
        loadButton.setBounds(500, 900, 90, 40);
        frame.add(loadButton);

        insertButton = new JButton("Wstaw");
        insertButton.setBounds(600, 900, 90, 40);
        frame.add(insertButton);

        readButton = new JButton("Odczytaj");
        readButton.setBounds(700, 900, 90, 40);
        frame.add(readButton);

        saveButton = new JButton("Zapisz");
        saveButton.setBounds(800, 900, 90, 40);
        frame.add(saveButton);

        textAreaLabel = new JLabel("Podaj tekst");
        textAreaLabel.setBounds(230, 780, 70,30);
        frame.add(textAreaLabel);

        textArea = new JTextArea();
        textArea.setBounds(300, 750, 400, 100);
        frame.add(textArea);

        passwordFieldLabel = new JLabel("Podaj haslo");
        passwordFieldLabel.setBounds(930, 770, 70,20);
        frame.add(passwordFieldLabel);

        passwordField = new JTextField();
        passwordField.setBounds(1000, 770, 100, 20);
        frame.add(passwordField);

        keyFieldLabel = new JLabel("Podaj klucz");
        keyFieldLabel.setBounds(930, 810, 70,20);
        frame.add(keyFieldLabel);

        keyField = new JTextField();
        keyField.setBounds(1000, 810, 100, 20);
        frame.add(keyField);

        // no idea why it is needed
        SwingUtilities.updateComponentTreeUI(frame);

        loadButton.addActionListener(e -> {
            int openResult = fileChooser.showOpenDialog(panel);
            if (openResult == JFileChooser.APPROVE_OPTION) {
                selectedFile = fileChooser.getSelectedFile();
                img = null;
                try {
                    img = ImageIO.read(selectedFile);

                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                icon = new ImageIcon(img);
                label = new JLabel(icon);
                label.setBounds(50, 50, 600, 600);
                frame.add(label);
                SwingUtilities.updateComponentTreeUI(frame);
            }
        });

    }
}
